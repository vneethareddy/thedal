import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BloodbanksListPage } from './bloodbanks-list';

@NgModule({
  declarations: [
    BloodbanksListPage,
  ],
  imports: [
    IonicPageModule.forChild(BloodbanksListPage),
  ],
})
export class BloodbanksListPageModule {}
