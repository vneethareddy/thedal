import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  tab1Root = ContactPage;  
  tab2Root = HomePage; 
  tab3Root = HomePage; 

  constructor(public navCtrl: NavController) {

  }

}
