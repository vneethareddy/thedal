import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddblooddonorPage } from './addblooddonor';

@NgModule({
  declarations: [
    AddblooddonorPage,
  ],
  imports: [
    IonicPageModule.forChild(AddblooddonorPage),
  ],
})
export class AddblooddonorPageModule {}
